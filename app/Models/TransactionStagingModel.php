<?php

namespace App\Models;

use App\Models\BaseModel;

class TransactionStagingModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'transaction_staging';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'client_id',
        'transaction_details',
        'transaction_status'
    ];

    public $hidden = [];

    public $rules = [
        'client_id' => 'sometimes|required',
        'transaction_details' => 'sometimes|required',
        'transaction_status' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
