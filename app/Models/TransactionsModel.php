<?php

namespace App\Models;

use App\Models\BaseModel;

class TransactionsModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'transaction';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'client_id',
        'transaction_id',
        'product_id',
        'package_id',
        'payment_status',
        'payment_from'
    ];

    public $hidden = [];

    public $rules = [
        'client_id' => 'sometimes|required',
        'transaction_id' => 'sometimes|required',
        'product_id' => 'sometimes|required',
        'package_id' => 'sometimes|required',
        'payment_status' => 'sometimes|required',
        'payment_from' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
