<?php

namespace App\Models;

use App\Models\BaseModel;

class ScheduleModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'token_scheduler';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'token_id',
        'expiration',
        'price'
    ];

    public $hidden = [];

    public $rules = [
        'token_id' => 'sometimes|required',
        'expiration' => 'sometimes|required',
        'price' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
