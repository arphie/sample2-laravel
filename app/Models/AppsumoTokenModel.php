<?php

namespace App\Models;

use App\Models\BaseModel;

class AppsumoTokenModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'appsumo_tokens';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'code',
        'email',
        'status'
    ];

    public $hidden = [];

    public $rules = [
        'code' => 'sometimes|required',
        'email' => 'sometimes|required',
        'status' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
