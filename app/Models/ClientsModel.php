<?php

namespace App\Models;

use App\Models\BaseModel;

class ClientsModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'clients';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'email'
    ];

    public $hidden = [];

    public $rules = [
        'first_name' => 'sometimes|required',
        'last_name' => 'sometimes|required',
        'email' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
