<?php

namespace App\Models;

use App\Models\BaseModel;

class TokenModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'api_tokens';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'client_id',
        'domain',
        'token',
        'product_package',
        'transaction_id',
        'status'
    ];

    public $hidden = [];

    public $rules = [
        'client_id' => 'sometimes|required',
        'domain' => 'sometimes|required',
        'token' => 'sometimes|required',
        'product_package' => 'sometimes|required',
        'transaction_id' => 'sometimes|required',
        'status' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
