<?php

namespace App\Models;

use App\Models\BaseModel;

class OcTransactionsModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'oc_transactions';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'order_id',
        'order_type',
        'activation_count',
        'invoice_no',
        'store_id',
        'customer_id',
        'firstname',
        'lastname',
        'email',
        'price',
        'product_id',
        'product_name'
    ];

    public $hidden = [];

    public $rules = [
        'order_id' => 'sometimes|required',
        'order_type' => 'sometimes|required',
        'activation_count' => 'sometimes|required',
        'invoice_no' => 'sometimes|required',
        'store_id' => 'sometimes|required',
        'customer_id' => 'sometimes|required',
        'firstname' => 'sometimes|required',
        'lastname' => 'sometimes|required',
        'email' => 'sometimes|required',
        'price' => 'sometimes|required',
        'product_id' => 'sometimes|required',
        'product_name' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
