<?php

namespace App\Models;

use App\Models\BaseModel;

class ActivationsModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'activations';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'subscription',
        'transaction_id',
        'token',
        'product',
        'domain',
        'email',
        'client_id',
        'status'
    ];

    public $hidden = [];

    public $rules = [
        'subscription' => 'sometimes|required',
        'transaction_id' => 'sometimes|required',
        'token' => 'sometimes|required',
        'product' => 'sometimes|required',
        'domain' => 'sometimes|required',
        'email' => 'sometimes|required',
        'client_id' => 'sometimes|required',
        'status' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
