<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductsModel extends BaseModel
{
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'products';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'name',
        'description',
        'image',
        'price'
    ];

    public $hidden = [];

    public $rules = [
        'product_id' => 'sometimes|required',
        'name' => 'sometimes|required',
        'description' => 'sometimes|required',
        'image' => 'sometimes|required',
        'price' => 'sometimes|required',
    ];

    public function transactions()
     {
         return $this->morphMany();
     }
}
