<?php

namespace App\Http\Services\Access;

use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;
use Session;

class ChangeDomainService extends BaseService
{   
    private $token;

    public function __construct(
        TokenRepository $tokenrepo
    ){
        $this->token = $tokenrepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $this->token->updateToken($data);
        $redirect_to = env('APP_URL').'/admin/licenses';
        return redirect($redirect_to);

        exit;
    }

}
