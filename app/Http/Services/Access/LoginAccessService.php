<?php

namespace App\Http\Services\Access;

use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;
use App\Models\User;
use Session;

class LoginAccessService extends BaseService
{   
    private $token;

    public function __construct(
        TokenRepository $tokenrepo
    ){
        $this->token = $tokenrepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $login_details = [
            "email" => $data['username'],
            "password" => $data['password']
        ];

        if($data['username'] == "admin" && $data['password'] == "admin123"){
            Session::put('user_admin', 1);
            $admininfo = [];
            $admininfo['success']['name'] = 'admin';

            $final_info = (object) $admininfo;
            $final_info = json_encode($final_info);
            Session::put('user_info', $final_info);
            $redirect_to = env('APP_URL').'/admin/licenses';
            return redirect($redirect_to);
            exit;
        }

        $user_info = User::where([['email', "=", $data['username']],['password', "=", $data['password']]])->first();
        if($user_info !== null){
            $user_information = [
                'success' => [
                    'id' => $user_info->id,
                    'name' => $user_info->name,
                    'email' => $data['username'],
                ] 
            ];

            Session::put('auth_token', "temporary");
            Session::put('user_info', json_encode($user_information));
            
            $redirect_to = env('APP_URL').'/purchases';
            return redirect($redirect_to);
            exit;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://accounts.ibial.com/api/v1/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($login_details));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        
        curl_close ($ch);

        $login_response = json_decode($server_output);

        if(isset($login_response->success)){
            // // check if user is client

            // // check if user is admin
            // Storage::disk('local')->put($myfile, $postdata);
            // dump($login_response->success->token);
            Session::put('auth_token', $login_response->success->token);
            
            $authorization = "Authorization: Bearer ".$login_response->success->token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://accounts.ibial.com/api/v1/getUserInfo");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            
            curl_close ($ch);

            Session::put('user_info', $server_output);
        
            // $login_response = json_decode();



            

            
            // Session::put('auth_token', $login_response->success->token);
            $redirect_to = env('APP_URL').'/purchases';
            return redirect($redirect_to);
        } else {
            // dump($login_response->error);
            $redirect_to = env('APP_URL').'/login?status=failed';
            return redirect($redirect_to);
        }

        // dump($login_response);
        // Session::put('user_client', 1);
        // return redirect('/purchases');
        exit;
    }

}
