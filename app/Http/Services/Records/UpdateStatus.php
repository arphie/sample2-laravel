<?php

namespace App\Http\Services\Records;

use App\Http\Repositories\TransactionsRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class UpdateStatus extends BaseService
{   
    private $repo;
    private $token;

    public function __construct(
        TransactionsRepository $transactionRepo,
        TokenRepository $tokenRepo
    ){
        $this->repo = $transactionRepo;
        $this->token = $tokenRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($data)
    {   
        $token_info = $this->token->update_status($data);
    }

}
