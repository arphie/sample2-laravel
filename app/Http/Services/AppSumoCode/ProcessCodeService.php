<?php

namespace App\Http\Services\AppSumoCode;


use App\Http\Repositories\AppSumoCodeRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Services\BaseService;

class ProcessCodeService extends BaseService
{   
    private $code;
    private $token;

    public function __construct(
        AppSumoCodeRepository $appSumoRepo,
        TokenRepository $tokenRepo
    ){
        $this->code = $appSumoRepo;
        $this->token = $tokenRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($data)
    { 
        // check token validity
        if(!$this->code->checkTokenValidity($data['appcode'])){
            // if token is not valid
            return view("convert", ['error' => ['message' => 'Invalid Token']]);
        }
    
        // check token availability
        if(!$this->code->checkTokenAvailability($data['appcode'])){
            // if token is not available
            return view("convert", ['error' => ['message' => 'The code you entered is already in use']]);
        }

        // update token DB
        $this->code->updateTokenStatus($data['appcode']);

        // prepare transaction data
        $transaction_insertion = [
            "order_id" => '0',
            "order_type" => 'non-reseller',
            "activation_count" => 1,
            "invoice_no" => $data['appcode'],
            "store_id" => 1,
            "customer_id" => 1,
            "firstname" => 'Ibial',
            "lastname" => 'Client',
            "email" => $data['demail'],
            "price" => 1,
            "product_id" => 50,
            "product_name" => 'MadChatter',
            "reference_token" => '001'
        ];
        
        // insert transacition and insert new user if email is not yet a user
        $token_data = $this->token->insert_from_oc($transaction_insertion);
        
        // insert license
        $id = $token_data['data']['id'];
        $this->token->inject_activation($transaction_insertion, $id);

        $user_infomation = $this->code->getUserInfo($data['demail']);

        // return view("convertsuccess", ['success' => ['message' => 'Token Activated']]);
        if(!empty($user_infomation)){
            $redirect_to = env('APP_URL').'/redeem/success?email='.$user_infomation['email'].'&token='.$user_infomation['password'];
        } else {
            $redirect_to = env('APP_URL').'/redeem/success';
        }
        
        return redirect($redirect_to);
    }

}
