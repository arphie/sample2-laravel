<?php

namespace App\Http\Services\AppSumoCode;


use App\Http\Repositories\ResellerTokenRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Services\BaseService;

class GetResellerTokensService extends BaseService
{   
    private $code;
    private $token;

    public function __construct(
        ResellerTokenRepository $resellerRepo,
        TokenRepository $tokenRepo
    ){
        $this->code = $resellerRepo;
        $this->token = $tokenRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($email)
    { 
        return $this->code->getTransactions($email);
    }

}
