<?php

namespace App\Http\Services\AppSumoCode;


use App\Http\Repositories\AppSumoCodeRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Services\BaseService;

class PullCodesService extends BaseService
{   
    private $code;
    private $token;

    public function __construct(
        AppSumoCodeRepository $appSumoRepo,
        TokenRepository $tokenRepo
    ){
        $this->code = $appSumoRepo;
        $this->token = $tokenRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle()
    { 
        $tokens = $this->code->getAllCodes();
        return $tokens;


        // dump("im at services");
        // // pull tokens

        

        // dump($tokens);
    }

}
