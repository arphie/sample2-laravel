<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\ClientRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class InsertService extends BaseService
{   
    private $client;

    public function __construct(
        ClientRepository $clientrepo
    ){
        $this->client = $clientrepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $insert_data = $this->client->insert($data);
        return $this->absorb($insert_data);
    }

}
