<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\TransactionsRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class TransactionProceedService extends BaseService
{   
    private $repo;
    private $token;

    public function __construct(
        TransactionsRepository $transactionRepo,
        TokenRepository $tokenRepo
    ){
        $this->repo = $transactionRepo;
        $this->token = $tokenRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($data)
    {   
        $token_data = $this->repo->proceedTransactions($data);

        if($token_data['status'] == 500){
            return $this->absorb($token_data);
        }

        // $token_data['data']['transaction'] = 10;
        $data['transaction_ref'] = $token_data['data']['transaction'];

        $make_tokens = $this->token->generateMultipleTokens($data);
        return $this->absorb($make_tokens);
    }

}
