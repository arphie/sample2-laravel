<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\ResellerTokenRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class InsertFromOcService extends BaseService
{   
    private $token;

    public function __construct(
        ResellerTokenRepository $resellerRepo,
        TokenRepository $tokenrepo
    ){
        $this->token = $tokenrepo;
        $this->code = $resellerRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        if($data['order_type'] == 'reseller'){
            $data['activation_count'] = $this->code->resellerActivationCount($data['email'], $data['activation_count']);
        }
        
        $token_data = $this->token->insert_from_oc($data);
        // insert activation
        $id = $token_data['data']['id'];
        $this->token->inject_activation($data, $id);
        
        return $this->absorb($token_data);
    }

}
