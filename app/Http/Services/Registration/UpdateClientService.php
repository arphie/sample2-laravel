<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\ClientRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class UpdateClientService extends BaseService
{   
    private $client;

    public function __construct(
        ClientRepository $clientRepo
    ){
        $this->client = $clientRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($data)
    {   
        $token_data = $this->client->update($data);
        return $this->absorb($token_data);
    }

}
