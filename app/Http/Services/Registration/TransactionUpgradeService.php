<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\TransactionsRepository;
use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class TransactionUpgradeService extends BaseService
{   
    private $repo;
    private $token;

    public function __construct(
        TransactionsRepository $transactionRepo,
        TokenRepository $tokenRepo
    ){
        $this->repo = $transactionRepo;
        $this->token = $tokenRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($data)
    {      
        // update token packages
        $upgrade = $this->token->update_token_package($data);

        // update transactions
        $transaction = $this->repo->proceedTransactions($data);
        return $this->absorb($transaction);
    }

}
