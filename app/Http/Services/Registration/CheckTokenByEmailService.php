<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class CheckTokenByEmailService extends BaseService
{   
    private $token;

    public function __construct(
        TokenRepository $tokenrepo
    ){
        $this->token = $tokenrepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $token_data = $this->token->check_by_email($data);
        // return $this->absorb($token_data);
    }

}
