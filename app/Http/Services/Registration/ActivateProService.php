<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\TokenRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class ActivateProService extends BaseService
{   
    private $token;

    public function __construct(
        TokenRepository $tokenrepo
    ){
        $this->token = $tokenrepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle(array $data)
    {   
        $token_data = $this->token->activate_pro($data);
        return $this->absorb($token_data);
    }

}
