<?php

namespace App\Http\Services\Registration;

use App\Http\Repositories\TransactionsRepository;

use App\Http\Repositories\ProjectsRepository;
use App\Http\Services\BaseService;

class TransactionSaveService extends BaseService
{   
    private $repo;

    public function __construct(
        TransactionsRepository $transactionRepo
    ){
        $this->repo = $transactionRepo;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function handle($data)
    {   
        $token_data = $this->repo->saveTransactions($data);
        return $this->absorb($token_data);
    }

}
