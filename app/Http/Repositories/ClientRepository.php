<?php


namespace App\Http\Repositories;

use App\Models\ClientsModel;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class ClientRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $client;

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        ClientsModel $clientModel
    ){
        $this->client = $clientModel;
    }

    public function insert($data)
    {
        $client = $this->client->init($data);

        if (!$client->validate($data)) {
            $errors = $client->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while validating the Client Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        //region Data insertion 
        if (!$client->save()) {
            $errors = $client->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the Client Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the Client Details.',
            'data' => [
                'project' => $client->id,
            ],
        ];
    }

    public function update($data)
    {
        $client_info = $this->client->find($data['client_id']);

        // if not found, return false
        if (!$client_info) {
            return [
                'status' => 400,
                'message' => 'Client Details not found',
                'data' => [],
            ];
        }

        // unset id
        if (isset($data['client_id'])) {
            unset($data['client_id']);
        }

        $client_info->fill($data);

        //region Data insertion
        if (!$client_info->save()) {
            $errors = $client_info->getErrors();
            return [
                'status' => 500,
                'message' => 'Something went wrong.',
                'data' => $errors,
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully updated the Projects.',
            'data' => $data,
        ];

    }

    
}
