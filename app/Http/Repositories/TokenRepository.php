<?php


namespace App\Http\Repositories;

use App\Models\TokenModel;
use App\Models\OcTransactionsModel;
use App\Models\ActivationsModel;
use App\Models\User;

use App\Http\Repositories\BaseRepository;

use Exception;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class TokenRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $token;
    private $oc_token;
    private $activation;
    private $users;

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        TokenModel $tokenModel,
        OcTransactionsModel $ocToken,
        ActivationsModel $activationModel,
        User $userModel
    ){
        $this->token = $tokenModel;
        $this->oc_token = $ocToken;
        $this->activation = $activationModel;
        $this->users = $userModel;
    }

    public function insert($data)
    {
        
        // check if domain exist
        if(isset($data['domain']) && $data['domain'] == ""){
            return [
                'status' => 500,
                'message' => 'Domain is Required',
                'data' => "",
            ];
        }

        // generate token
        $data['token'] = $this->generate_token($data['domain']);

        $token_info = $this->token->init($data);

        if (!$token_info->validate($data)) {
            $errors = $token_info->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while validating the Token Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        //region Data insertion 
        if (!$token_info->save()) {
            $errors = $token_info->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the Token Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the Token Details.',
            'data' => [
                'token' => $token_info->id,
            ],
        ];
    }

    public function check($token)
    {
        $token_info = $this->returnToArray($this->token->where("token", "=", $token["token"])->first());
        return [
            'status' => 200,
            'message' => 'Successfully fetched Token Details.',
            'data' => $token_info
        ];
    }

    public function all()
    {
        $token_info = $this->returnToArray($this->token->get());
        return [
            'status' => 200,
            'message' => 'Successfully fetched Token Details.',
            'data' => $token_info
        ];
    }

    public function generate_token($simple_string)
    {
        // $to_concode = $simple_string."::".date("Y-m-d H:i:s");
        $to_concode = $simple_string;

        // Store the cipher method 
        $ciphering = "AES-128-CTR"; 
        
        // Use OpenSSl Encryption method 
        $iv_length = openssl_cipher_iv_length($ciphering); 
        $options = 0; 
        
        // Non-NULL Initialization Vector for encryption 
        $encryption_iv = '0123456789abcdef'; 
        
        // Store the encryption key 
        $encryption_key = "ibial_token_generate"; 
        
        // Use openssl_encrypt() function to encrypt the data 
        $encryption = openssl_encrypt($simple_string, $ciphering, $encryption_key, $options, $encryption_iv); 
        
        // Display the encrypted string 
        // echo "Encrypted String: " . $encryption . "\n"; 

        return $encryption;

    }

    public function break_token($string)
    {
        // Store the cipher method 
        $ciphering = "AES-128-CTR"; 
        
        // Use OpenSSl Encryption method 
        $iv_length = openssl_cipher_iv_length($ciphering); 
        $options = 0; 
        
        // Non-NULL Initialization Vector for encryption 
        $encryption_iv = '0123456789abcdef'; 
        
        // Store the encryption key 
        $encryption_key = "ibial_token_generate"; 
        
        // Use openssl_encrypt() function to encrypt the data 
        $decryption = openssl_decrypt($string, $ciphering, $encryption_key, $options, $encryption_iv); 
        
        // Display the encrypted string 
        // echo "Encrypted String: " . $encryption . "\n"; 

        return $decryption;
    }

    public function generateMultipleTokens($data)
    {
        // transaction_id::transaction_ref::date::count

        $all_token = [];

        for ($i=1; $i <= $data['number_of_token']; $i++) { 
            // build token
            $build_value = $data['transaction_id']."::".$data['transaction_ref']."::".date("Y-m-d H:i:s")."::".$i;
            $dtoken = $this->generate_token($build_value);

            $data['token'] = $dtoken;
            $data['product_package'] = $data['package_id'];
            // initiate token
            $token_info = $this->token->init($data);

            if (!$token_info->validate($data)) {
                $errors = $token_info->getErrors();
                array_push($all_token, [
                    'status' => 500,
                    'message' => 'An error has occurred while validating the Token Details.',
                    'meta' => [
                        'errors' => (array) $errors,
                    ],
                    'data' => (array) $data
                ]);
            }

            //region Data insertion 
            if (!$token_info->save()) {
                $errors = $token_info->getErrors();
                array_push($all_token, [
                    'status' => 500,
                    'message' => 'An error has occurred while saving the Token Details.',
                    'meta' => [
                        'errors' => $errors,
                    ],
                    'data' => $data
                ]);
            }

            array_push($all_token, [
                'status' => 200,
                'message' => 'Successfully saved the Token Details.',
                'meta' => [
                    'token' => $token_info->id,
                ],
                'data' => $data
            ]);
        }
        
        return [
            'status' => 200,
            'message' => 'Successfully saved the Token Details.',
            'data' => $all_token
        ];
    }


    public function update_token_package($data)
    {
        $get_token_id = $this->token->where('token', '=', $data['token'])->update(['product_package' => $data['package_id']]);
    }

    public function get_tokens($data)
    {
        $dtokens = $this->returnToArray($this->token->where('client_id', '=', $data['client_id'])->get());

        return [
            'status' => 200,
            'message' => 'Successfully fetched the Token Details.',
            'data' => $dtokens
        ];
    }

    public function tokens_all_segrigated()
    {
        $dtokens = $this->returnToArray($this->activation->get());

        

        $final_actives = [];
        $final_actives['all'] = $dtokens;
        foreach($dtokens as $dkey => $dvalue){
            $final_actives[$dvalue['status']] = [];
        }
        
        // assign data
        foreach($dtokens as $dfkey => $dfvalue){
            array_push($final_actives[$dfvalue['status']], $dfvalue);
        }
        
        return $final_actives;
    }

    public function update_status($data)
    {
        // $this->token->where('id', $data['id'])->update(['status' => $data['update_to']]);
        $this->activation->where('id', $data['id'])->update(['status' => $data['update_to']]);
    }

    public function check_by_email($data)
    {
        dump($data);
        # code...
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function insert_from_oc($data)
    {


        // dump($data);

        // check accounts if user exist

        $email = [
            'email' => $data['email']
        ];

        $ch = curl_init();
        $query_build = http_build_query($email);
        $url = "https://accounts.ibial.com/api/v1/user/email?".$query_build;

        // comment this part on live
        // =========================== BOF certificate
        // $certificate_location = 'C:\Users\Celym\Downloads\cacert.pem';
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        // ============================ EOF certificate

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);

        // if ($server_output === false) {
        //     throw new Exception(curl_error($ch), curl_errno($ch));
        // }

        curl_close ($ch);
        $login_response = json_decode($server_output);

        $response = $login_response->message;

        // build email body
        $user_info_base = [];

        if($response == "Email found"){ 
            // create link to licenses
            // dump("email found");
            $user_info_base = [
                "name" => 'Ibial User',
                "email" => $data['email'],
                "password" => ""
            ];

        } else {
            // register user as temporary account
            // dump("email not found");

            // check if user exist on temporary storage
            $user_info = $this->users->where('email', "=", $data['email'])->first();
            if(empty($user_info)){
                // insert a new temporary

                $newpass = $this->randomPassword();

                $user_info_base = [
                    "name" => 'Ibial User',
                    "email" => $data['email'],
                    "password" => $newpass
                ];
                $this->users->name = 'Ibial User';
                $this->users->email = $data['email'];
                $this->users->password = $newpass;
                $this->users->save();

                $body = "Temporary user inserted";


            } else {
                // use current account
                // dump($this->returnToArray($user_info));
                $temp_user = $this->returnToArray($user_info);
                $user_info_base = [
                    "name" => $temp_user['name'],
                    "email" => $temp_user['email'],
                    "password" => $temp_user['password']
                ];
            }

        }

        // dump($user_info_base);

        // launch Welcome email

        $send_mail = curl_init();
        $url='https://mail.ibial.com/api/sendemail';
        curl_setopt($send_mail, CURLOPT_URL, $url );
        curl_setopt($send_mail, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($send_mail, CURLOPT_POST, 1);
        $html = '
        <table style="width: 100%;text-align: center;">
            <tbody>
                <tr>
                    <td>
                        <table style="width: 80%;margin: 0 auto;text-align: left;">
                            <tr>
                                <td><img src="https://ibial.com/licenses/images/ibial-logo.svg"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <table style="width: 80%;text-align: center;margin: 0 auto;border: 1px solid #e4e2e2;padding: 25px 0;border-radius: 5px;">
                        <tr>
                            <td>
                                <table style="width: 100%;text-align: center;">
                                    <tr>
                                        <td style="font-weight: bold;font-size: 30px;text-transform: uppercase;">Welcome to IBial</td>
                                    </tr>
                                </table>
                                <table style="width: 100%;text-align: center;">
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%;text-align: center;">
                                    <tr>
                                        <td style="font-size: 14px;padding-bottom: 15px;">Congratulations on your new purchase!</td>
                                    </tr>
                                </table>
                                <table style="width: 100%;text-align: center;">
                                    <tr>
                                        <td><a style="background: #7b68ee;color: #fff;padding: 15px 25px;display: inline-block;font-size: 18px;text-decoration: none;border-radius: 5px;font-weight: lighter;" href="'.env('APP_URL').'/login?user='.$user_info_base['email'].'&token='.$user_info_base['password'].'">Click here to view your purchase</a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </tr>
            </tbody>
        </table>';

        

        $html = preg_replace('/"/',"'",$html);
        $html = preg_replace("/\r|\n/", "", $html);
        $html = preg_replace("/\r|\t/", "", $html);
        // dump($html);

        $message_body = '{
            "hasattachment" : "no",
            "email_body" : "'. html_entity_decode($html, ENT_QUOTES, 'UTF-8') . '",
            "recipientemail" : "'.$user_info_base['email'].'",
            "recipientname" : "'.$data['firstname'].' '.$data['lastname'].'",
            "senderemail" : "support@ibial.com",
            "sendername" : "iBial Support",
            "subject" : "Welcome to Ibial"
            }';

        // dump($data);
        curl_setopt($send_mail, CURLOPT_POSTFIELDS, $message_body );

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($send_mail, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($send_mail);
        if (curl_errno($send_mail)) {
            dump(curl_error($send_mail));
        }

        // dump($result);

        curl_close($send_mail);

        $token_info = $this->oc_token->init($data);

        if (!$token_info->validate($data)) {
            $errors = $token_info->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while validating the Transaction Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        //region Data insertion 
        if (!$token_info->save()) {
            $errors = $token_info->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the Transaction Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the Transaction Details.',
            'data' => [
                'id' => $token_info->id,
            ],
        ];
        
    }


    public function activate_free($data)
    {
        if($this->check_email_for_activation($data['email'])){
            return [
                'status' => 500,
                'message' => 'Email is already been used',
                'data' => "",
            ];
        }

        // initialize info
        $data['transaction_id'] = "none";
        $data['subscription'] = "free";
        $data['token'] = "none";
        $data['product'] = "MadChatter Free";
        // $data['domain'] = "none";
        $data['client_id'] = 0;
        $data['status'] = 'active';
        
        $token_info = $this->activation->init($data);

        if (!$token_info->validate($data)) {
            $errors = $token_info->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while validating the Activation Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        //region Data insertion 
        if (!$token_info->save()) {
            $errors = $token_info->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the Activation Details.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the Activation Details.',
            'data' => [
                'id' => $token_info->id,
            ],
        ];
    }

    public function activate_pro($data)
    {
        // dump($data);

        // // get transaction as per email
        // $get_transactions = $this->returnToArray($this->oc_token->where("email", "=", $data['email'])->get());

        // $get_transaction_ids = [];
        // foreach ($get_transactions as $key => $value) {
        //     array_push($get_transaction_ids, $value['id']);
        // }

        // dump($get_transaction_ids);


        // exit;
        $token_info = $this->returnToArray($this->activation->where("token", "=", $data['token'])->first());

        if(empty($token_info)){
            return [
                'status' => 500,
                'message' => 'Token not found',
                'data' => [],
            ];
        }

        if($token_info['domain'] != "none"){
            return [
                'status' => 500,
                'message' => 'Token is already taken',
                'data' => [],
            ];
        }

        $this->activation->where('token', '=', $data['token'])->update([
            'domain' => $data['domain'],
            'status' => "active"
        ]);

        return [
            'status' => 200,
            'message' => 'Successfully upgraded to pro version',
            'data' => [],
        ];
    }

    public function check_email_for_activation($email)
    {
       $email_exist = $this->returnToArray($this->activation->where("email", "=", $email)->first());
       return (!empty($email_exist) ? true : false);
    }

    public function monitor($data)
    {
        
        // $monitor_info = $this->returnToArray($this->activation->where([
        //     ["email", "=", $data['email']],
        //     ["domain", "=", $data['domain']],
        //     ["subscription", "=", $data['subscription']]
        // ])->first());

        $monitor_info = $this->returnToArray($this->activation->where("domain", "=", $data['domain'])->first());

        if(!empty($monitor_info)){
            
            if($monitor_info['status'] == "inactive"){
                return [
                    'status' => 500,
                    'message' => 'Domain not found',
                    'data' => "",
                ];
            } else {
                return [
                    'status' => 200,
                    'message' => 'Account Validated',
                    'data' => "",
                ];
            }            
        } else {
            return [
                'status' => 500,
                'message' => 'Domain not found',
                'data' => "",
            ];
        }
    }

    public function inject_activation($data, $id)
    {
        $number_of_injections = $data['activation_count'];

        for ($i=0; $i < $number_of_injections; $i++) { 
            $token_build = $id."::".$data['product_name']."::".date("Y-m-d H:i:s")."::".$i;
            $dtoken = $this->generate_token($token_build);

            $xinfo = [];
            $xinfo['domain'] = "none";
            $xinfo['email'] = $data['email'];
            $xinfo['transaction_id'] = $id;
            $xinfo['token'] = $dtoken;
            $xinfo['subscription'] = "pro";
            $xinfo['product'] = $data['product_name'];
            $xinfo['client_id'] = $data['customer_id'];
            $xinfo['status'] = 'available';
            
            $token_info = $this->activation->init($xinfo);

            if (!$token_info->validate($xinfo)) {
                $errors = $token_info->getErrors();
                // dump($errors);
            }

            //region Data insertion 
            if (!$token_info->save()) {
                $errors = $token_info->getErrors();
                // dump($errors);
            }
        }
    }

    public function updateToken($data)
    {
        // print_r($data);
        $this->activation->where('id', $data['transactionid'])->update(['domain' => $data['newdomain']]);

    }

    public function get_user_info($data)
    {
        if(@$data['email'] == ""){
            return [
                'status' => 500,
                'message' => 'No email found',
                'data' => "",
            ];    
        }

        $email = [
            'email' => $data['email']
        ];

        $ch = curl_init();
        $query_build = http_build_query($email);
        $url = "https://accounts.ibial.com/api/v1/user/email?".$query_build;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);\
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $server_output = curl_exec($ch);
        // if ($server_output === false) {
        //     throw new Exception(curl_error($ch), curl_errno($ch));
        // }
    
        curl_close ($ch);

        $server_output = json_decode($server_output);
        $response = $server_output->message;

        // dump($response);

        // if account is inside the accounts
        if($response == "Email found"){
            return [
                'status' => 200,
                'message' => 'Account is validated',
                'data' => [
                    'email' => $data['email']
                ],
            ];
        }

        // check if account is inside the licensing
        $get_user_info = $this->returnToArray($this->users->where('email', '=', $data['email'])->first());
        
        // check if user is inside the licensing
        if(!empty($get_user_info)){
            return [
                'status' => 200,
                'message' => 'Account is not validated',
                'data' => [
                    'email' => $data['email']
                ],
            ];
        }

        return [
            'status' => 500,
            'message' => 'Domain not found',
            'data' => "",
        ];

    }
    
}
