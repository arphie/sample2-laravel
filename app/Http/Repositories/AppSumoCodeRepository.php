<?php


namespace App\Http\Repositories;

use App\Models\AppsumoTokenModel; // initiate token model
use App\Models\User;

// Base Repository
use App\Http\Repositories\BaseRepository;

// Base Level Functions
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class AppSumoCodeRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $token;
    private $users;

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        AppsumoTokenModel $appSumoTokenModel,
        User $userModel
    ){
        $this->token = $appSumoTokenModel;
        $this->users = $userModel;
    }

    public function checkTokenValidity($token)
    {
        $token_information = $this->returnToArray($this->token->where("code", "=", $token)->first());

        // if token is available
        if(!empty($token_information)){
            return true;
        }

        // if token is already taken
        return false;
    }

    public function checkTokenAvailability($token)
    {
        $token_information = $this->returnToArray($this->token->where("code", "=", $token)->first());

        // if token is available
        if($token_information['status'] == "available"){
            return true;
        }

        // if token is already taken
        return false;
    }

    public function updateTokenStatus($token)
    {
        $this->token->where("code", "=", $token)->update(['status' => 'taken']);
    }

    public function getAllCodes()
    {
        $all_tokens = [];
        $token_info = $this->returnToArray($this->token->all());
        foreach ($token_info as $key => $value) {
            array_push($all_tokens, $value['code']);
        }
        return $all_tokens;
    }

    public function getUserInfo($email)
    {
        $info = $this->returnToArray($this->users->where('email', '=', $email)->first());

        return $info;
    }

    
    
    
}
