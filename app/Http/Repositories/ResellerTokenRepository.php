<?php


namespace App\Http\Repositories;

use App\Models\ResellerTokenModel; // initiate token model
use App\Models\User;
use App\Models\OcTransactionsModel;

use App\Models\ActivationsModel;

// Base Repository
use App\Http\Repositories\BaseRepository;

// Base Level Functions
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class ResellerTokenRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $token;
    private $users;
    private $activation;
    private $octransactions;

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        ResellerTokenModel $ResellerTokenModel,
        ActivationsModel $activationModel,
        User $userModel,
        OcTransactionsModel $OCTransactions
    ){
        $this->token = $ResellerTokenModel;
        $this->users = $userModel;
        $this->activation = $activationModel;
        $this->octransactions = $OCTransactions;
    }

    public function checkTokenValidity($token)
    {
        $token_information = $this->returnToArray($this->token->where("code", "=", $token)->first());

        // if token is available
        if(!empty($token_information)){
            return true;
        }

        // if token is already taken
        return false;
    }

    public function checkTokenAvailability($token)
    {
        $token_information = $this->returnToArray($this->token->where("code", "=", $token)->first());

        // if token is available
        if($token_information['status'] == "available"){
            return true;
        }

        // if token is already taken
        return false;
    }

    public function updateTokenStatus($email, $token)
    {
        $this->token->where("code", "=", $token)->update(['status' => 'taken', 'email' => $email]);
    }

    public function inject_activation($data, $id)
    {
        $number_of_injections = $data['activation_count'];
        
        for ($i=0; $i < $number_of_injections; $i++) { 
            $token_build = $id."::".$data['product_name']."::".date("Y-m-d H:i:s")."::".$i;
            $dtoken = $this->generate_token($token_build);

            $xinfo = [];
            $xinfo['domain'] = "none";
            $xinfo['email'] = $data['email'];
            $xinfo['transaction_id'] = $id;
            $xinfo['token'] = $dtoken;
            $xinfo['subscription'] = "pro";
            $xinfo['product'] = $data['product_name'];
            $xinfo['client_id'] = $data['customer_id'];
            $xinfo['status'] = 'available';
            
            $token_info = $this->activation->init($xinfo);

            if (!$token_info->validate($xinfo)) {
                $errors = $token_info->getErrors();
                // dump($errors);
            }

            //region Data insertion 
            if (!$token_info->save()) {
                $errors = $token_info->getErrors();
                // dump($errors);
            }
        }
    }

    // stacking algorithm
    public function stackingAlgo($token_count)
    {
        $dolddiff = 0;
        
        // build stacking licenses algo
        if($token_count == 0){
            $dolddiff = 0;
        } elseif($token_count == 1){
            $dolddiff = 2;
        } elseif($token_count == 2){
            $dolddiff = 3;
        } elseif ($token_count == 3){
            $dolddiff = 5;
        } else {
            $final_count = 0;
            $base = 10;
            $dolddiff = 5;
            for ($i=1; $i <= $token_count; $i++) {
                if($i > 3){
                    $dolddiff = $dolddiff + 1;
                }
            }
        }

        return $dolddiff;
    }

    // 
    public function getLicenses($token_count)
    {
        $licenses = 0;
        if($token_count > 0){
            if($token_count == 1){
                $licenses = 2;
            } else if($token_count == 2){
                $licenses = 5;
            } else if($token_count == 3){
                $licenses = 10;
            } else {
                $licenses = 10; 
                $def = 5;
                for ($i = 1; $i <= $token_count; $i++) {
                    if($i > 3){
                        $def = $def + 1;
                        $licenses = $licenses + $def;
                    }
                }
            }
        }
        return $licenses;
    }

    public function getActivationCount($email)
    {
        // check number of active tokens
        // $tokens = $this->returnToArray($this->token->where("email", "=", $email)->get());

        // update to allow ibial store 
        // get number of reseller on oc transactions
        $tokens = $this->returnToArray($this->octransactions->where([["order_type", "=", 'reseller'],["email", "=", $email]])->get());

        $token_count = count($tokens) + 1;
        
        $activation_counter = $this->stackingAlgo($token_count);

        return $activation_counter;
    }

    public function resellerActivationCount($email, $activations)
    {
        // check number of active tokens
        // $tokens = $this->returnToArray($this->token->where("email", "=", $email)->get());

        // get number of reseller on oc transactions
        $tokens = $this->returnToArray($this->octransactions->where([["order_type", "=", 'reseller'],["email", "=", $email]])->get());

        // get current licenses
        $current_licenses = $this->getLicenses(count($tokens));

        $projected_count = count($tokens) + $activations;
        $projected_licenses = $this->getLicenses($projected_count);

        $new_licenses = $projected_licenses - $current_licenses;

        return $new_licenses;
    }

    public function getUserInfo($email)
    {
        $info = $this->returnToArray($this->users->where('email', '=', $email)->first());

        return $info;
    }

    public function getTransactions($email)
    {
        $info = $this->returnToArray($this->octransactions->where('email', '=', $email)->get());

        $ocids = [];
        foreach ($info as $key => $value) {
            if($value['order_type'] == "reseller"){
                array_push($ocids, $value['id']);
            }
        }

        $reseller_Tokens = $this->returnToArray($this->activation->whereIn('transaction_id', $ocids)->get());

        return $reseller_Tokens;
    }
    
    
    
    
    
}
