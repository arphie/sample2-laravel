<?php


namespace App\Http\Repositories;

use App\Models\TransactionsModel; // transactions 
use App\Models\TransactionStagingModel; // saving transactions

use App\Models\OcTransactionsModel; // saving transactions
use App\Models\ActivationsModel; // activations

use App\Models\TokenModel; // initiate token 

use App\Models\ProductsModel; // initiate Products  

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

/**
 * Class FundRepository
 *
 * @package App\Data\Repositories\Users
 */
class TransactionsRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $transaction;
    private $transactionStaging;
    private $oc_transactions;
    private $activation;
    private $products;

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        TransactionsModel $transactionModel,
        TransactionStagingModel $transactionStagingModel,
        OcTransactionsModel $ocTransactionModel,
        ActivationsModel $activationModel,
        ProductsModel $productsModel
    ){
        $this->transaction = $transactionModel;
        $this->transactionStaging = $transactionStagingModel;
        $this->oc_transactions = $ocTransactionModel;
        $this->activation = $activationModel;
        $this->products = $productsModel;
    }
    
    public function saveTransactions($data)
    {   
        $data['transaction_details'] = serialize($data['transaction']);
        $data['transaction_status'] = "onprogress";
        $transactioninfo = $this->transactionStaging->init($data);
        
        if (!$transactioninfo->validate($data)) {
            $errors = $transactioninfo->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the transaction',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        if (!$transactioninfo->save()) {
            $errors = $transactioninfo->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the transaction.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the transaction.',
            'data' => [
                'token' => $transactioninfo->id,
            ],
        ];

    }

    public function proceedTransactions($data)
    {
        $transac_full = $this->transaction->init($data);
        
        if (!$transac_full->validate($data)) {
            $errors = $transac_full->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the transaction',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        if (!$transac_full->save()) {
            $errors = $transac_full->getErrors();
            return [
                'status' => 500,
                'message' => 'An error has occurred while saving the transaction.',
                'data' => [
                    'errors' => $errors,
                ],
            ];
        }

        return [
            'status' => 200,
            'message' => 'Successfully saved the transaction.',
            'data' => [
                'transaction' => $transac_full->id,
            ],
        ];
    }

    public function get_transactions($data)
    {
        $transactions = $this->returnToArray($this->transaction->where("client_id", "=", $data['client_id'])->get());

        return [
            'status' => 200,
            'message' => 'Successfully fetched the transaction.',
            'data' => [
                'transaction' => $transactions,
            ],
        ];
    }

    public function get_transactions_from_oc($email)
    {
        # code...
        $dtransactions = $this->returnToArray($this->oc_transactions->where("email", "=", $email)->get());
        return $dtransactions;
    }

    public function one_transaction($id)
    {
        $dtransactions = $this->returnToArray($this->oc_transactions->where("id", "=", $id)->first());

        // get activations 
        $activations = $this->returnToArray($this->activation->where("transaction_id", "=", $id)->get());
        $dtransactions['activations'] = $activations;

        return $dtransactions;
    }

    public function get_transactions_info($data)
    {
        // dump($data);
        // "type": "reseller"


        if(isset($data['email']) && $data['email'] != ""){
            if(isset($data['type']) && $data['type'] != ""){
                $transactions = $this->returnToArray($this->oc_transactions->where([['order_type', '=', $data['type']],['email', '=', $data['email']]])->get());
                foreach ($transactions as $key => $value) {
                    $product_info = $this->returnToArray($this->products->where('product_id', '=', $value['product_id'])->first());
                    $transactions[$key]['product_info'] = $product_info;
                }
            } else {
                $transactions = $this->returnToArray($this->oc_transactions->where('email', '=', $data['email'])->get());
                foreach ($transactions as $key => $value) {
                    $product_info = $this->returnToArray($this->products->where('product_id', '=', $value['product_id'])->first());
                    $transactions[$key]['product_info'] = $product_info;
                }
            }
            
            return [
                'status' => 200,
                'message' => 'Successfully fetched the transaction.',
                'data' => [
                    'transaction' => $transactions,
                ],
            ];
        } else {
            return [
                'status' => 500,
                'message' => 'No email found',
                'data' => [],
            ];
        }

        exit;

    }
    
    
}
