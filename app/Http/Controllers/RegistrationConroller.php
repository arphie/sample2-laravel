<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\Registration\InsertService;
use App\Http\Services\Registration\CreateTokenService;
use App\Http\Services\Registration\CheckTokenService;
use App\Http\Services\Registration\TokenGetAllService;
use App\Http\Services\Registration\UpdateClientService;
use App\Http\Services\Registration\TransactionSaveService;
use App\Http\Services\Registration\TransactionProceedService;
use App\Http\Services\Registration\TransactionUpgradeService;
use App\Http\Services\Registration\TokenForViewService;
use App\Http\Services\Registration\CheckTokenByEmailService;
use App\Http\Services\Registration\InsertFromOcService;
use App\Http\Services\Registration\MonitorProductService;

use App\Http\Services\Registration\ActivateFreeService;
use App\Http\Services\Registration\ActivateProService;

class RegistrationConroller extends Controller
{
    public function add(
        Request $request,
        InsertService $insert
    )
    {
        $data = $request->all();
        return $insert->handle($data);
    }

    public function update(
        Request $request,
        $id, 
        UpdateClientService $update
    )
    {
        $data = $request->all();
        $data['client_id'] = $id;
        return $update->handle($data);
    }

    public function token_create(
        Request $request,
        CreateTokenService $token
    )
    {
        $data = $request->all();
        return $token->handle($data);
    }

    public function token_check(
        Request $request,
        CheckTokenService $token
    )
    {
        $data = $request->all();
        return $token->handle($data);
    }

    public function token_check_email(
        Request $request,
        CheckTokenByEmailService $token
    )
    {
        $data = $request->all();
        return $token->handle($data);
    }

    public function token_all(
        TokenGetAll $token
    )
    {
        return $token->handle();
    }

    public function transaction_save(
        Request $request,
        TransactionSaveService $transac
    )
    {
        $data = $request->all();
        return $transac->handle($data);
    }

    public function insert_from_oc(
        Request $request,
        InsertFromOcService $transac
    )
    {
        $data = $request->all();
        return $transac->handle($data);
    }


    public function transaction_proceed(
        Request $request,
        TransactionProceedService $transac
    )
    {
        $data = $request->all();
        return $transac->handle($data);
    }

    public function improve(
        Request $request,
        TransactionUpgradeService $transac
    )
    {
        
        $data = $request->all();    
        return $transac->handle($data);
    }

    public function view_tokens(
        TokenForViewService $dtokens
    )
    {
        $licenses = $dtokens->handle();
        return view("admin.licenses", ["licenses"=>$licenses]);
        // return "test";
    }

    public function monitor(
        Request $request,
        MonitorProductService $monitor
    )
    {
        $data = $request->all();
        return $monitor->handle($data);
    }

    public function free_activate(
        Request $request,
        ActivateFreeService $activatefree
    )
    {
        $data = $request->all();
        return $activatefree->handle($data);
    }

    public function pro_activate(
        Request $request,
        ActivateProService $activatepro
    )
    {
        $data = $request->all();
        return $activatepro->handle($data);
    }

}
