<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\Access\LoginAccessService;
use App\Http\Services\Access\ChangeDomainService;
use App\Models\User;
use Session;

class AccountsController extends Controller
{
    
    public function login(
        Request $request
    )
    {
        $data = $request->all();

        if(isset($data['user']) && $data['token']){
            $user_info = User::where([['email', "=", $data['user']],['password', "=", $data['token']]])->first();

            if($user_info !== null){
    
                $user_information = [
                    'success' => [
                        'id' => $user_info->id,
                        'name' => $user_info->name,
                        'email' => $data['user'],
                    ] 
                ];
    
                Session::put('auth_token', "temporary");
                Session::put('user_info', json_encode($user_information));
                
                $redirect_to = env('APP_URL').'/purchases';
                return redirect($redirect_to);
            } else {
                return view("login");
            }
        } else {
            return view("login");
        }

        
        
        
    }

    public function forgot()
    {
        return view("forgot");
    }

    public function process_forgot(
        Request $request
    )
    {
        $data = $request->all();

        $email = [
            'email' => $data['demail']
        ];

        $ch = curl_init();
        $query_build = http_build_query($email);
        $url = "https://accounts.ibial.com/api/v1/user/email?".$query_build;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);

        $server_output = json_decode($server_output);
        $response = $server_output->message;

        if($response != "Email found"){
            return view("forgot", ['error' => ['message' => 'Email not Found']]);
        }

        

        echo "<pre>";
        print_r($server_output);
        echo "</pre>";



        // $send_mail = curl_init();
        // $url='https://mail.ibial.com/api/sendemail';
        // curl_setopt($send_mail, CURLOPT_URL, $url );
        // curl_setopt($send_mail, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($send_mail, CURLOPT_POST, 1);
        // $html = '
        // <table style="width: 100%;text-align: center;">
        //     <tbody>
        //         <tr>
        //             <td><img src="https://ibial.com/licenses/images/ibial-logo.svg"></td>
        //         </tr>
        //         <tr>
        //             <table style="width: 80%;text-align: center;margin: 0 auto;border: 1px solid #e4e2e2;padding: 25px 0;border-radius: 5px;">
        //                 <tr>
        //                     <td>
        //                         <table style="width: 100%;text-align: center;">
        //                             <tr>
        //                                 <td style="font-weight: bold;font-size: 30px;text-transform: uppercase;">Lost Password</td>
        //                             </tr>
        //                         </table>
        //                         <table style="width: 100%;text-align: center;">
        //                             <tr>
        //                                 <td>&nbsp;</td>
        //                             </tr>
        //                         </table>
        //                     </td>
        //                 </tr>
        //                 <tr>
        //                     <td>
        //                         <table style="width: 100%;text-align: center;">
        //                             <tr>
        //                                 <td style="font-size: 14px;padding-bottom: 15px;">Congratulations on your new purchase!</td>
        //                             </tr>
        //                         </table>
        //                         <table style="width: 100%;text-align: center;">
        //                             <tr>
        //                                 <td><a style="background: #7b68ee;color: #fff;padding: 15px 25px;display: inline-block;font-size: 18px;text-decoration: none;border-radius: 5px;font-weight: lighter;" href="'.env('APP_URL').'/login?user='.$user_info_base['email'].'&token='.$user_info_base['password'].'">Click here to view your purchase</a></td>
        //                             </tr>
        //                         </table>
        //                     </td>
        //                 </tr>
        //             </table>
        //         </tr>
        //     </tbody>
        // </table>';

        

        // $html = preg_replace('/"/',"'",$html);
        // $html = preg_replace("/\r|\n/", "", $html);
        // $html = preg_replace("/\r|\t/", "", $html);
        // // dump($html);

        // $message_body = '{
        //     "hasattachment" : "no",
        //     "email_body" : "'. html_entity_decode($html, ENT_QUOTES, 'UTF-8') . '",
        //     "recipientemail" : "'.$user_info_base['email'].'",
        //     "recipientname" : "'.$data['firstname'].' '.$data['lastname'].'",
        //     "senderemail" : "support@ibial.com",
        //     "sendername" : "iBial Support",
        //     "subject" : "Welcome to Ibial"
        //     }';

        // // dump($data);
        // curl_setopt($send_mail, CURLOPT_POSTFIELDS, $message_body );

        // $headers = array();
        // $headers[] = 'Content-Type: application/json';
        // curl_setopt($send_mail, CURLOPT_HTTPHEADER, $headers);

        // $result = curl_exec($send_mail);
        // if (curl_errno($send_mail)) {
        //     dump(curl_error($send_mail));
        // }

        // // dump($result);

        // curl_close($send_mail);

        dump($data);
    }

    public function logout()
    {
        Session::flush();

        $redirect_to = env('APP_URL').'/login';
        return redirect($redirect_to);
    }

    public function process_from_form(
        Request $request,
        LoginAccessService $login
    )
    {
        $data = $request->all();
        return $login->handle($data);
        
    }

    public function change_domain(
        Request $request,
        ChangeDomainService $change
    )
    {
        $data = $request->all();
        return $change->handle($data);
    }
    
}
