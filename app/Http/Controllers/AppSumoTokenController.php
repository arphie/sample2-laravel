<?php

namespace App\Http\Controllers;

use App\Http\Services\AppSumoCode\ProcessCodeService;
use App\Http\Services\AppSumoCode\ResellerTokenProcessService;
use App\Http\Services\AppSumoCode\PullCodesService;
use Illuminate\Http\Request;

use App\Models\ActivationsModel;

use Response;

class AppSumoTokenController extends Controller
{
    //
    public function convertionPage()
    {
        return view("convert");
    }

    public function validationPage()
    {
        return view("validation");
    }

    // add validation
    public function processValidationPage(
        Request $request,
        ActivationsModel $activationModel
    )
    {
        $data = $request->all();

        $tokenInfo = $activationModel::where('token', "=", $data['dtoken'])->first();

        $return_message = [];
        $return_message['token'] =  $data['dtoken'];

        if($tokenInfo === NULL){
            $return_message['message'] = "Token does not exist";
        } else {
            $tokenInfo = $tokenInfo->toArray();

            if($tokenInfo['status'] == 'available'){
                $return_message['message'] = "Token is available";
            }
    
            if($tokenInfo['domain'] != 'none'){
                $return_message['message'] = "Token is already in use";
            }
    
        }

        

        return view("validation", ["info" => $return_message]);
    }

    public function processConvertion(
        Request $request,
        ProcessCodeService $processService,
        ResellerTokenProcessService $resellerService
    )
    {
        $data = $request->all();

        // check if token is reseller or non-reseller
        if(strpos($data['appcode'], '-rs-') !== false){
            return $resellerService->handle($data);
        } else {
            return $processService->handle($data);
        }
    }

    public function processSuccess()
    {
        return view("convertsuccess");
    }

    public function showpassform(Type $var = null)
    {
        return view("exportcodes");
    }

    public function pullAppCodes(
        Request $request,
        PullCodesService $pullCodes
    )
    {

        $data = $request->all();
        // print_r($data);

        if($data['cxpasssword'] != "XMTExZKUkJ22fa5f"){
            // return view("exportcodes");

            $redirect_to = env('APP_URL').'/pullAppCodes';
            return redirect($redirect_to);
        }

        $tokens = $pullCodes->handle();

        header('Content-Disposition: attachment; filename="export.csv"');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary\n");

        $out = fopen('php://output', 'w');
        foreach($tokens as $line)
        {
            fputcsv($out, array($line));
        }
        fclose($out);

        
        dump("download here");
        exit;
    }
}
