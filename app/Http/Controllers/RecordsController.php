<?php

namespace App\Http\Controllers;

use App\Http\Services\Records\GetTokensByUsers;
use App\Http\Services\Records\GetTransactionsByUsers;
use App\Http\Services\Records\OcTransactionsServices;
use App\Http\Services\Records\TransactionFromOcServices;
use App\Http\Services\Records\UserInfoServices;
use App\Http\Services\Records\TransactionInfomationService;

use App\Http\Services\AppSumoCode\GetResellerTokensService;

use App\Http\Services\Records\UpdateStatus;

use App\Models\User;

use Response;
use Illuminate\Http\Request;
use Session;


class RecordsController extends Controller
{

    public function tokens(
        Request $request,
        GetTokensByUsers $tokens
    )
    {
        $data = $request->all();
        return $tokens->handle($data);
    }

    public function transaction(
        Request $request,
        GetTransactionsByUsers $transactions
    )
    {
        $data = $request->all();
        return $transactions->handle($data);
    }

    public function update_status(
        $id,
        $update_to,
        UpdateStatus $update
    )
    {
        $data['id'] = $id;
        $data['update_to'] = $update_to;

        $tokenchange = $update->handle($data);
        $redirect_to = env('APP_URL').'/admin/licenses';
        return redirect($redirect_to);
    }

    public function update_status_client(
        $id,
        $page_id,
        $update_to,
        UpdateStatus $update
    )
    {
        $data['id'] = $id;
        $data['update_to'] = $update_to;

        $tokenchange = $update->handle($data);
        $redirect_to = env('APP_URL').'/purchases/'.$page_id;

        return redirect($redirect_to);
    }

    public function transaction_info(
        OcTransactionsServices $transactions
    )
    {
        $data = [];
        $user_information = json_decode(session('user_info'));
        $data['email'] = $user_information->success->email;

        $transac = $transactions->handle($data);

        return view("client.purchases", ["transactions"=> $transac]);
    }

    public function view_transaction(
        $id,
        TransactionFromOcServices $transactionfromOC
    )
    {
        $transac = $transactionfromOC->handle($id);
        return view("client.transactions", ["transactions"=> $transac]);
    }

    public function validate_info()
    {
        return view("client.validate");
    }

    public function validate_info_data(
        Request $request
    )
    {
        $data = $request->all();

        // if password did not match
        if($data['dpassword'] != $data['dcpassword']){
            return view("client.validate", ['error' => ['form_error' => true, 'message' => 'Username and Password did not match']]);
            exit;
        }

        $register_data = [
            "name" => $data['username'],
            "email" => $data['demail'],
            "password" => $data['dpassword'],
            "c_password" => $data['dcpassword']
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://accounts.ibial.com/api/v1/register");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($register_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        
        curl_close ($ch);

        $login_response = json_decode($server_output);

        if(isset($login_response->success)){
            // // check if user is client

            // delete local info
            $user_info = User::where('email', "=", $data['demail'])->delete();

            // // check if user is admin
            // Storage::disk('local')->put($myfile, $postdata);
            // dump($login_response->success->token);
            Session::put('auth_token', $login_response->success->token);
            
            $authorization = "Authorization: Bearer ".$login_response->success->token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://accounts.ibial.com/api/v1/getUserInfo");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            
            curl_close ($ch);

            Session::put('user_info', $server_output);
            
            // Session::put('auth_token', $login_response->success->token);
            $redirect_to = env('APP_URL').'/purchases';
            return redirect($redirect_to);
        } else {
            // dump($login_response->error);
            $redirect_to = env('APP_URL').'/login?status=failed';
            return redirect($redirect_to);
        }
        exit;
    }

    public function download_product(
        $product_id
    )
    {

        $path = public_path()."/products/";
        $filename = "";

        // if($product_id == 1){
            $filename = "madchatter-pro.zip";
        // } else {
        //     $redirect_to = env('APP_URL').'/purchases';
        //     return redirect($redirect_to);
        //     exit;
        // }

        $fileUrl = $path."".$filename;

        if (file_exists($fileUrl)) {
            // print_r("file found");
            ob_end_clean();
            return Response::download($fileUrl, $filename, array('Content-Type: application/octet-stream','Content-Length: '. filesize($fileUrl)))->deleteFileAfterSend(false);
        } else {
            // print_r("file not found");
            $redirect_to = env('APP_URL').'/purchases';
            return redirect($redirect_to);
        }

        exit;
    }

    public function viewResellers(
        GetResellerTokensService $getResellerTokens
    )
    {
        $sessions_all = Session::get('user_info');
        $sessions_all = json_decode($sessions_all);

        $reseller_tokens = $getResellerTokens->handle($sessions_all->success->email);
        




        return view("resellers", ["resellers"=> $reseller_tokens]);
    }

    public function user_info(
        Request $request,
        UserInfoServices $userInformation
    )
    {
        $data = $request->all();
        $transac = $userInformation->handle($data);
        return $transac;
    }

    public function transaction_information(
        Request $request,
        TransactionInfomationService $transactionInformation
    )
    {
        $data = $request->all();
        $trnsac_info = $transactionInformation->handle($data);
        return $trnsac_info;
    }
}
