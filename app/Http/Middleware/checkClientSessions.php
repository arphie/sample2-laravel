<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class checkClientSessions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!session('auth_token')) {
            // user value cannot be found in session
            $redirect_to = env('APP_URL').'/login';
            return redirect($redirect_to);
        }
        return $next($request);
    }
}
