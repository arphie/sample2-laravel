@extends('layouts.pages.clients')

@section('page_title', 'Resellers')

<?php
    $user_info = json_decode(session('user_info'));
?>

@section('page_desc')
    <div class="dclient_content">
        {{-- <h2>Welcome, <?php echo $user_info->success->name; ?>!</h2> --}}
        {{-- <div class="into_desc">this is the itntro desc</div> --}}
    </div>
@endsection

@section('content')
    <div class="dclient_purchases">
        <table id="example" class="ddatatable table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Product</th>
                    <th>Status</th>
                    <th>Token</th>
                    <th>Created at</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($resellers as $key => $value): ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $value['product']; ?></td>
                        <td><?php echo $value['status']; ?></td>
                        <td>
                            <div class="dtoken">
                                <div class="dlefts">
                                    <div class="dpartial"><?php echo "******************************".substr($value['token'], -10); ?></div>
                                    <div class="dfull"><?php echo $value['token']; ?></div>
                                </div>
                                <div class="drights">
                                    <div class="dviewbotton">
                                        <a>show code</a>
                                    </div>
                                </div>
                            </div>
                            
                        </td>
                        <td><?php echo date('F d, Y', strtotime($value['created_at'])); ?></td>
                    </tr>
                <?php endforeach; ?>
                
            </tbody>
        </table>
    </div>
@endsection

@section('custom_script')
    <script>
        $(document).ready(function() {
            $('#products').DataTable({
                paging: false
            });

            $(".dtoken .drights a").click(function(e){
                e.preventDefault();

                if($(this).parents('.dtoken').find('.dfull').hasClass('opened')){
                    $(this).parents('.dtoken').find('.dfull').removeClass('opened').hide();
                    $(this).parents('.dtoken').find('.dpartial').show();
                    $(this).text('Show Code');
                } else {
                    $(this).parents('.dtoken').find('.dfull').addClass('opened').show();
                    $(this).parents('.dtoken').find('.dpartial').hide();
                    $(this).text('Hide Code');
                }
            });

            $('#example').DataTable();
        } );
    </script>
@endsection

