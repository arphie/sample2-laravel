@extends('layouts.pages.fullpage')

@section('content')
<div class="login-page">
    <div class="login-inner">
        <div class="login-form">
            {{-- <h2>Login</h2> --}}
            <?php if(isset($error)): ?>
                <div class="error_info">
                    Something went wrong, Please try again
                </div>
            <?php endif; ?>
            <form action="<?php echo env('APP_URL'); ?>/login/forgot" method="post">
                {{ csrf_field() }}
                <div class="login-item">
                    <input type="text" name="demail" placeholder="Email">
                </div>
                <div class="login-item">
                    <input type="submit" name="subs" placeholder="Submit">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
