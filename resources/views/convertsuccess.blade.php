@extends('layouts.pages.convert')

@section('content')
<div class="redeem-page">
    <div class="redeem-inner">
        <div class="redeem-header">
            <div class="redeem-logo">
                <img src="{{ asset('images/ibial_appsumo.jpg') }}" alt="">
            </div>
            <div class="redeem-desc">
                Hey Sumo-lings! Let’s set up your iBial account and AppSumo Code and you’ll<br />be ready to get your own live chat running!
            </div>
        </div>
        <div class="redeem-form">
            <div class="dsucecss-message">
                <div class="dicon"><i class="far fa-check-circle"></i></div>
                <div class="dmessage">Success! Confirmation details will be sent to your email shortly.</div>
                <?php if(isset($_GET['email']) && isset($_GET['token'])): ?>
                    <div class="dgoto"><a href="<?php echo env('APP_URL'); ?>/login?user=<?php echo @$_GET['email']; ?>&token=<?php echo @$_GET['token']; ?>">Go to Dashboard <i class="fas fa-angle-double-right"></i></a></div>
                <?php else: ?>
                    <div class="dgoto"><a href="<?php echo env('APP_URL'); ?>/login">Go to Dashboard <i class="fas fa-angle-double-right"></i></a></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

@endsection
