@extends('layouts.pages.main')

@section('page_title', 'Products')

@section('content')
    <?php
        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://ibial.com/store/index.php?route=api/product/getProducts");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            $server_output = json_decode($server_output);

            curl_close ($ch);
    ?>
    <table id="products" class="products_table table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th style="width:30px;">No</th>
                <th>Products</th>
                <th style="width:70px;">Created</th>
                <th style="width:70px;">Status</th>
                <th style="width:90px;">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($server_output as $sokey => $sovalue): ?>
                <tr>
                    <td><?php echo $sovalue->product_id; ?></td>
                    <td>
                        <span class="dproductmain"><?php echo $sovalue->name; ?></span>
                    </td>
                    <td><?php echo date("F j, Y", strtotime($sovalue->date_added)); ?></td>
                    <td>Active</td>
                    <td>
                        <ul class="dactions">
                            <li><a href="#" data-toggle="modal" data-target="#openDesc<?php echo $sovalue->product_id; ?>"><i class="fas fa-eye"></i></a></li>
                            {{-- <li><a href="#"><i class="fas fa-trash"></i></a></li> --}}
                        </ul>
                        <div class="modal fade" id="openDesc<?php echo $sovalue->product_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel"><?php echo $sovalue->name; ?></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="product_info">
                                        <?php echo html_entity_decode($sovalue->description); ?>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
@endsection

@section('custom_script')
    <script>
        $(document).ready(function() {
            $('#products').DataTable({
                paging: false
            });
        } );
    </script>
@endsection
