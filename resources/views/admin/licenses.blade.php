@extends('layouts.pages.main')

@section('page_title', 'Licenses')

@section('content')
    <nav>
        <div class="main_tab nav nav-tabs" id="nav-tab" role="tablist">
            <?php foreach ($licenses as $menukey => $menuvalue): ?>
                <a class="nav-item nav-link <?php echo ($menukey == 'all' ? 'active' : ''); ?>" id="nav-<?php echo $menukey; ?>-tab" data-toggle="tab" href="#nav-<?php echo $menukey; ?>" role="tab" aria-controls="nav-<?php echo $menukey; ?>" aria-selected="true"><?php echo $menukey; ?> (<?php echo count($menuvalue)?>)</a>
            <?php endforeach; ?>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        
        <?php foreach ($licenses as $itemkey => $itemvalue): ?>
            <div class="tab-pane fade <?php echo ($itemkey == 'all' ? 'active show' : ''); ?>" id="nav-<?php echo $itemkey; ?>" role="tabpanel" aria-labelledby="nav-<?php echo $itemkey; ?>-tab">
                <table id="table-<?php echo $itemkey; ?>" class="products_table table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Products</th>
                            <th>Customer Email</th>
                            <th style="width:70px;">Domain</th>
                            <th>Created</th>
                            <th style="width:70px;">Status</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php foreach ($itemvalue as $itmkey => $itmvalue): ?>
                            <tr>
                                <td><?php echo $itmvalue['subscription']; ?></td>
                                <td><?php echo $itmvalue['product']; ?></td>
                                <td><?php echo $itmvalue['email']; ?></td>
                                <td><?php echo $itmvalue['domain']; ?></td>
                                <td><?php echo date('F d, Y', strtotime($itmvalue['created_at'])); ?></td>
                                <td><?php echo $itmvalue['status']; ?></td>
                                <td>
                                    <ul class="dactions">
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#license<?php echo $itmvalue['id']; ?>"><i class="far fa-eye"></i></a>
                                            <div class="modal fade dlicensemodal" id="license<?php echo $itmvalue['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="license_<?php echo $itmvalue['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                  <div class="modal-content dlicensemodal">
                                                    <div class="modal-header">
                                                      <h5 class="modal-title" id="license_<?php echo $itmvalue['id']; ?>"><?php echo $itmvalue['product']; ?></h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body">
                                                      <div class="modal_content">
                                                          <div class="row">
                                                              <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Token</label>
                                                                    <div class="token-view"><?php echo $itmvalue['token']; ?></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Product Name</label>
                                                                    <input type="text" class="form-control" value="<?php echo $itmvalue['product']; ?>" disabled>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Date Created</label>
                                                                    <input type="text" class="form-control" value="<?php echo date('F d, Y', strtotime($itmvalue['created_at'])); ?>" disabled>
                                                                </div>
                                                                {{-- <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Expiry Date</label>
                                                                    <input type="text" class="form-control" value="none" disabled>
                                                                </div> --}}
                                                              </div>
                                                              <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Domain</label>
                                                                    <input type="text" class="form-control" value="<?php echo $itmvalue['domain']; ?>" disabled>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Status</label>
                                                                    <input type="text" class="form-control" value="<?php echo $itmvalue['status']; ?>" disabled>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Customer Email</label>
                                                                    <input type="text" class="form-control" value="<?php echo $itmvalue['email']; ?>" disabled>
                                                                </div>
                                                                {{-- <div class="form-group">
                                                                    <label for="exampleFormControlInput1">Max. Activation Count</label>
                                                                    <input type="text" class="form-control" value="--" disabled>
                                                                </div> --}}
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="modal fade dlicensemodal" id="changedomain<?php echo $itmvalue['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="changedomain<?php echo $itmvalue['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                  <div class="modal-content dlicensemodal">
                                                    <div class="modal-header">
                                                      <h5 class="modal-title" id="license_<?php echo $itmvalue['id']; ?>">Change Domain</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body">
                                                      <div class="modal_content">
                                                          <div class="row">
                                                              <div class="col-md-12">
                                                                  <div class="d-change-domain-note">You are about to change the domain <strong><?php echo $itmvalue['domain']; ?></strong></div>
                                                                  <form action="<?php echo env('APP_URL'); ?>/admin/licenses" method="POST">
                                                                    {{ csrf_field() }}
                                                                    <div class="formitem">
                                                                        <input type="text" name="newdomain" placeholder="Enter new Domain" required>
                                                                        <input type="hidden" value="<?php echo $itmvalue['id']; ?>" name="transactionid">
                                                                        <input type="hidden" value="<?php echo $itmvalue['domain']; ?>" name="olddomain">
                                                                    </div>
                                                                    <div class="formitem" style="text-align: right;">
                                                                        <input type="submit" name="cdsubs" value="change">
                                                                    </div>
                                                                  </form>
                                                              </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                        </li>
                                        <li class="ddroplc">
                                            <a href="#"><i class="fas fa-ellipsis-v"></i></a>
                                            <ul class="lcdropdown">
                                                <li><a href="#"  data-toggle="modal" data-target="#changedomain<?php echo $itmvalue['id']; ?>">Change Domain</a></li>
                                                <?php if($itmvalue['status'] == "active"): ?>
                                                    <li><a href="<?php echo env('APP_URL'); ?>/admin/licenses/<?php echo $itmvalue['id']; ?>/inactive">Deactivate</a></li>
                                                <?php else: ?>
                                                    <li><a href="<?php echo env('APP_URL'); ?>/admin/licenses/<?php echo $itmvalue['id']; ?>/active">Activate</a></li>
                                                <?php endif; ?>
                                                
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endforeach; ?>
    </div>
@endsection

@section('custom_script')
    <script>
        $(document).ready(function() {
            $('#table-all').DataTable({
                paging: false
            });

            $('#table-active').DataTable({
                paging: false
            });

            $('#table-available').DataTable({
                paging: false
            });

            $('#table-inactive').DataTable({
                paging: false
            });

            $('#table-expired').DataTable({
                paging: false
            });
            
        } );
    </script>
@endsection
