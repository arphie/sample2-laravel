<html>
    <head>
        <title>Licences | Ibial</title>
        <link data-n-head="1" rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap">
        <link href="{{ asset('css/global.css') }}" rel="stylesheet">
        
        <!-- BOF bootstrap CDN -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.min.css') }}">
        
        <!-- EOF bootstrap CDN -->
    </head>
    <body>
        <header>
            <?php
                $user_info = json_decode(session('user_info'));
                $user_token = session('auth_token');
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="dlogo">
                        <img src="{{ asset('images/ibial-logo.svg') }}" alt="">
                    </div>
                </div>
                <div class="col-md-6">&nbsp;</div>
                <div class="col-md-3">
                    <div class="admin-side">
                        <div class="dadmin-image">
                            {{-- <div class="dimageform"><img src="{{ asset('images/gal.jpg') }}" alt=""></div> --}}
                            <span><?php echo $user_info->success->name; ?> <a href="<?php echo env('APP_URL'); ?>/logout" class="signoutbutton">Sign out</a></span>
                        </div>
                        {{-- <div class="dadmin-dropdown">
                            <ul>
                                <li>item one</li>
                                <li>item two</li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
        </header>
        <?php
            if($user_token == "temporary"):
        ?>
            <div class="notifbar">
                <div class="notifbar_inner">
                    You are still using a temporary account, this might cause security issues. <a href="<?php echo env('APP_URL'); ?>/users/validate">Secure your account</a>
                </div>
            </div>
        <?php endif; ?>