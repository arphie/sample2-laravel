@include('layouts.sections.header')
    <div class="main_content">
        <div class="client content_main">
            <div class="client_inner">
                {{-- page title --}}
                <h1>@yield('page_title')</h1>
                @yield('page_desc')
                <div class="dmain-content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@include('layouts.sections.footer')