@include('layouts.sections.header')
    <div class="main_content">
        <div class="sidebar_main">
            @include('layouts.sections.sidebar')
        </div>
        <div class="content_main">
            {{-- page title --}}
            <h1>@yield('page_title')</h1>
            <div class="dmain-content">
                @yield('content')
            </div>
        </div>
    </div>
@include('layouts.sections.footer')