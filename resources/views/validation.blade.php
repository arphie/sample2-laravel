@extends('layouts.pages.convert')

@section('content')
<div class="redeem-page">
    <div class="redeem-inner">
        <div class="redeem-header">
            <div class="redeem-logo">
                <img src="{{ asset('images/ibial_appsumo.jpg') }}" alt="">
            </div>
            <div class="redeem-desc">
                Hey Sumo-lings! Try validating your token here and<br />be ready to get your own live chat running!
            </div>
        </div>
        <div class="redeem-form">
            

            
        
            <?php if(isset($info)): ?>
                <div class="dinformatopon">
                    <div class="dtoken"><input type="text" value="<?php echo $info['token']; ?>" disabled></div>
                    <div class="dmessafe"><?php echo $info['message']; ?></div>
                    <div class="dreser"><a href="<?php echo env('APP_URL'); ?>/validate/reseller">Try another token</a></div>
                </div>
                
            <?php else: ?>
                <form action="<?php echo env('APP_URL'); ?>/validate/reseller" method="post">
                    {{ csrf_field() }}
                    <div class="redeem-item">
                        <input type="text" name="dtoken" placeholder="Token">
                    </div>
                    <div class="redeem-item">
                        <input type="submit" name="subs" value="Validate">
                    </div>
                </form>
            <?php endif; ?>
        </div>
    </div>
</div>

@endsection
