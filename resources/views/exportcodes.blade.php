@extends('layouts.pages.convert')

@section('content')
<div class="redeem-page">
    <div class="redeem-inner">
        <div class="redeem-header">
            {{-- <div class="redeem-logo">
                <img src="{{ asset('images/ibial_appsumo.jpg') }}" alt="">
            </div> --}}
            {{-- <div class="redeem-desc">Export now!</div> --}}
        </div>
        <div class="redeem-form">
            <?php if(isset($error)): ?>
                <div class="derrorpart"><?php echo $error['message']; ?></div>
            <?php endif; ?>
            <form action="<?php echo env('APP_URL'); ?>/pullAppCodes" method="post">
                {{ csrf_field() }}
                <div class="redeem-item">
                    <input type="password" name="cxpasssword" placeholder="If you are one of us!">
                </div>
                <div class="redeem-item">
                    <input type="submit" name="subs" value="Pull Data">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
