@extends('layouts.pages.clients')

@section('page_title', 'Purchases')

<?php
    $user_info = json_decode(session('user_info'));
?>

@section('page_desc')
    <div class="dclient_content">
        <h2>Welcome, <?php echo $user_info->success->name; ?>!</h2>
        {{-- <div class="into_desc">this is the itntro desc</div> --}}
    </div>
@endsection

@section('content')
    <div class="dclient_purchases">
        <?php foreach ($transactions as $trkey => $trvalue): ?>
            <div class="dcp_items">
                <div class="dcp_inner">
                    <div class="row">
                        <div class="col-md-9">
                            <h2><?php echo $trvalue['product_name']; ?></h2>
                            <div class="itm_desc"><?php echo $trvalue['order_type']; ?></div>
                        </div>
                        <div class="col-md-3 dbuttonside">
                            <a href="<?php echo env('APP_URL'); ?>/purchases/<?php echo $trvalue['id']; ?>" class="btn btn-secondary">View Details</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
@endsection

@section('custom_script')
    <script>
        $(document).ready(function() {
            $('#products').DataTable({
                paging: false
            });
        } );
    </script>
@endsection
