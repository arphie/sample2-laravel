@extends('layouts.pages.details')

@section('page_title', 'Purchases')

@section('breadcrumbs')
    <ul>
        <li><a href="#"><i class="fas fa-arrow-left"></i> Back to Purchase</a></li>
    </ul>
@endsection

@section('content')
    <?php
        $licenses = [
            [
                'id' => 1,
                'status' => 'active',
                'activation_date' => '01-01-1992',
                'domain' => 'sample.com',
                'token' => 'asdasdqwacsawdasdcadad'
            ],
            [
                'id' => 2,
                'status' => 'active',
                'activation_date' => '01-01-1992',
                'domain' => 'sample.com',
                'token' => 'asdasdqwacsawdasdcadad'
            ]
        ];

        $payments = [
            [
                'id' => '0000001',
                'payment_date' => '01-01-1992',
                'type' => 'One-Time Payment',
                'ammount' => 50
            ],
            [
                'id' => '0000002',
                'payment_date' => '01-01-1992',
                'type' => 'One-Time Payment',
                'ammount' => 50
            ]
        ];
    ?>
    <div class="single_inner_content">
        <div class="si_item">
            <div class="si_item_inner">
                <h1>Mad Chatter Pro</h1>
                <div class="dsubinfo">
                    <ul>
                        <li>Status: <span class="isactive">Active</span></li>
                        <li>Activations: 1 of 2</li>
                    </ul>
                </div>
            </div>
            <div class="si_item_inner">
                <h2>License</h2>
                <div class="dtables">
                    <table id="products" class="products_table table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>License</th>
                                <th>Status</th>
                                <th style="width:150px;">Date Activated</th>
                                <th>Domain</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($licenses as $key => $livalue): ?>
                                <tr>
                                    <td><?php echo $livalue['id']; ?></td>
                                    <td><?php echo $livalue['status']; ?></td>
                                    <td><?php echo $livalue['activation_date']; ?></td>
                                    <td><?php echo $livalue['domain']; ?></td>
                                    <td><a href="">Deactivate</a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="si_item_inner">
                <h2>Payment History</h2>
                <div class="dtables">
                    <table id="products" class="products_table table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>License</th>
                                <th>Status</th>
                                <th style="width:100px;">Date Activated</th>
                                <th>Domain</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($payments as $key => $pyvalue): ?>
                                <tr>
                                    <td><?php echo $pyvalue['id']; ?></td>
                                    <td><?php echo $pyvalue['payment_date']; ?></td>
                                    <td><?php echo $pyvalue['type']; ?></td>
                                    <td><?php echo $pyvalue['ammount']; ?></td>
                                    <td><a href="">Get Invoice</a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_script')
    
@endsection
