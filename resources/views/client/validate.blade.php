@extends('layouts.pages.details')

@section('page_title', 'Purchases')

@section('breadcrumbs')
    <ul>
        <li><a href="<?php echo env('APP_URL'); ?>/purchases/"><i class="fas fa-arrow-left"></i> Back to Purchase</a></li>
    </ul>
@endsection

@section('content')
    <?php
        $user_info = json_decode(session('user_info'));
        $user_token = session('auth_token');
    ?>
    <div class="user_information">
        <div class="main_note">Update the following information to secure your account</div>
        <div class="info_main">
            
            <?php if(isset($error['form_error']) && $error['form_error'] == true): ?>
                <div class="error_message_form"><?php echo $error['message']; ?></div>
            <?php endif; ?>
            <form action="<?php echo env('APP_URL'); ?>/users/validate" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="username" class="col-form-label">Name</label>
                            <input type="text" class="form-control-plaintext" name="username" id="username" value="" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="demail" class="col-form-label">Email</label>
                            <input type="text" class="form-control-plaintext" name="demail" id="demail" value="<?php echo $user_info->success->email; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dpassword" class="col-form-label">Password</label>
                            <input type="password" class="form-control-plaintext" name="dpassword" id="dpassword" value="" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dcpassword" class="col-form-label">Confirm password</label>
                            <input type="password" class="form-control-plaintext" name="dcpassword" id="dcpassword" value="" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 dmain_submit">
                        <input type="hidden" name="user_email" value="<?php echo $user_info->success->email; ?>">
                        <input type="submit" class="subs">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('custom_script')
<script>
    $(document).ready(function() {
        // $('.doptions > i').click(function(event){
        //     // $(this).parent().find(".ddrop").show();
        //     console.log("here");

        //     $(this).parent().find(".ddrop").addClass("hgere");
        // });
    } );
</script>
@endsection
