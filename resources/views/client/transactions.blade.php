@extends('layouts.pages.details')

@section('page_title', 'Purchases')

@section('breadcrumbs')
    <ul>
        <li><a href="<?php echo env('APP_URL'); ?>/purchases/"><i class="fas fa-arrow-left"></i> Back to Purchase</a></li>
    </ul>
@endsection

@section('content')
    <?php
    

        $user_info = json_decode(session('user_info'));
        // count availables
        $activations = 0;
        foreach ($transactions['activations'] as $actkey => $actvalue) {
            if($actvalue['status'] == "available"){
                $activations++;
            }
        }
    ?>
    <?php if($user_info->success->email != $transactions['email']): ?>
    <div class="single_inner_content">
        <div class="si_item">
            <div class="si_item_inner">
                <h1 class="dtransacerror">Sorry, you are forbidden to access the page.</h1>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="single_inner_content">
        <div class="si_item">
            <div class="si_item_inner">
                <div class="downloadplugin">
                    <a href="<?php echo env('APP_URL'); ?>/purchases/download/<?php echo $transactions['product_id']; ?>" target="_blank">Download <?php echo $transactions['product_name']; ?></a>
                </div>
                <h1><?php echo $transactions['product_name']; ?></h1>
                <div class="dsubinfo">
                    <ul>
                        <li>Type: <span class="isactive" style="text-transform:capitalize;"><?php echo $transactions['order_type']; ?></span> <?php if($transactions['order_type'] == "reseller"): ?> (<a href="<?php echo env('APP_URL'); ?>/purchases/reseller/">View all reseller tokens</a>)<?php endif;?></li>
                        <li>Status: <span class="isactive">Active</span></li>
                        <li>Activations: <?php echo $activations; ?> of <?php echo $transactions['activation_count']; ?></li>
                    </ul>
                </div>
            </div>
            
            <div class="si_item_inner">
                <h2>License</h2>
                <div class="dtables">
                    <table id="products" class="products_table table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                {{-- <th>License</th> --}}
                                <th>Status</th>
                                <th style="width:100px;">License</th>
                                <th style="width:80px;">Date Activated</th>
                                <th>Domain</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transactions['activations'] as $key => $livalue): ?>
                                <tr>
                                    {{-- <td><?php echo $livalue['id']; ?></td> --}}
                                    <td style="text-transform: capitalize;"><?php echo $livalue['status']; ?></td>
                                    <td><?php echo "*****************".substr($livalue['token'], -5); ?></td>
                                    <td><?php echo date("F d, Y", strtotime($livalue['updated_at'])); ?></td>
                                    <td><?php echo $livalue['domain']; ?></td>
                                    <td>
                                        <div class="doptions">
                                            <i class="fas fa-ellipsis-h"></i>
                                            <div class="ddrop">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="modal"  data-target="#view_licence_<?php echo $livalue['id']; ?>">View Licence</a>
                                                    </li>
                                                    <?php if($livalue['status'] != "available"): ?>
                                                    <?php if($livalue['status'] == "active"): ?>
                                                        
                                                        <li><a href="<?php echo env('APP_URL'); ?>/licenses/<?php echo $livalue['id']; ?>/<?php echo $transactions['id']; ?>/inactive">Deactivate</a></li>
                                                    <?php else: ?>
                                                        <li><a href="<?php echo env('APP_URL'); ?>/licenses/<?php echo $livalue['id']; ?>/<?php echo $transactions['id']; ?>/active">Activate</a></li>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="view_licence_<?php echo $livalue['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLabel"><?php echo ($livalue['domain'] == "none" ? "No Assigned Domain Yet" : "Token for ".$livalue['domain']); ?></h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                  <div class="item_license"><?php echo $livalue['token']; ?></div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        <div class="modal fade" id="deactivate<?php echo $livalue['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                {{-- <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div> --}}
                                                <div class="modal-body">
                                                    <div class="deact_modal">
                                                        <div class="dm_middle">
                                                            <h2>Deactivate token for <?php echo $livalue['domain']; ?></h2>
                                                        </div>
                                                        <div class="dm_options">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="si_item_inner">
                <h2>Payment History</h2>
                <div class="dtables">
                    <table id="products" class="products_table table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Invoice no.</th>
                                <th>Email</th>
                                <th style="width:100px;">Date Purchased</th>
                                <th>Ammount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $transactions['invoice_no']; ?></td>
                                <td><?php echo $transactions['email']; ?></td>
                                <td><?php echo date("F d, Y", strtotime($transactions['updated_at'])); ?></td>
                                <td><?php echo number_format($transactions['price'], 2); ?></td>
                                <td>
                                    <a data-toggle="modal" data-target="#openInvoice" class="get_invoice">Get Invoice</a>
                                    <div class="modal fade" id="openInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header billing-modal">
                                              {{-- <h5 class="modal-title" id="exampleModalLongTitle">Invoice <?php echo $transactions['invoice_no']; ?></h5> --}}
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="bill-to-sectin col-md-6">
                                                        <h3>Bill To</h3>
                                                        <div class="inv_value full_name"><?php echo $transactions['lastname']; ?>, <?php echo $transactions['firstname']; ?></div>
                                                        <div class="inv_sub_val"><?php echo $transactions['email']; ?></div>
                                                    </div>
                                                    <div class="invoice-sector col-md-6">
                                                        <h3>Invoice</h3>
                                                        <div class="invoice-num"><?php echo $transactions['invoice_no']; ?></div>
                                                        <div class="invoice-num"><?php echo date("F d, Y", strtotime($transactions['created_at'])); ?></div>
                                                    </div>
                                                </div>
                                                <div class="order-list">
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <th>Description</th>
                                                                <th style="width:100px;">Type</th>
                                                                <th style="width:100px;">Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo $transactions['product_name']; ?></td>
                                                                <td><?php echo $transactions['order_type']; ?></td>
                                                                <td>₱<?php echo number_format($transactions['price'], 2, '.', ''); ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            {{-- <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="button" class="btn btn-primary">Save changes</button>
                                            </div> --}}
                                          </div>
                                        </div>
                                      </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
@endsection

@section('custom_script')
<script>
    $(document).ready(function() {
        // $('.doptions > i').click(function(event){
        //     // $(this).parent().find(".ddrop").show();
        //     console.log("here");

        //     $(this).parent().find(".ddrop").addClass("hgere");
        // });
    } );
</script>
@endsection
