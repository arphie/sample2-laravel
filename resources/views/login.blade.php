@extends('layouts.pages.fullpage')

@section('content')
<div class="login-page">
    <div class="login-inner">
        <div class="login-form">
            {{-- <h2>Login</h2> --}}
            <?php if(isset($_GET['status']) && $_GET['status'] == "failed"): ?>
                <div class="error_info">
                    Something went wrong, Please try again
                </div>
            <?php endif; ?>
            <form action="<?php echo env('APP_URL'); ?>/login" method="post">
                {{ csrf_field() }}
                <div class="login-item">
                    <input type="text" name="username" placeholder="Email">
                </div>
                <div class="login-item">
                    <input type="password" name="password" placeholder="Password">
                </div>
                <div class="login-item">
                    <input type="submit" name="subs" placeholder="Submit">
                </div>
                <div class="forgot-password">
                    {{-- <a href="<?php echo env('APP_URL'); ?>/login/forgot">Forgot Password?</a> --}}
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
