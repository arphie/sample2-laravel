@extends('layouts.pages.convert')

@section('content')
<div class="redeem-page">
    <div class="redeem-inner">
        <div class="redeem-header">
            <div class="redeem-logo">
                <img src="{{ asset('images/ibial_appsumo.jpg') }}" alt="">
            </div>
            <div class="redeem-desc">
                Hey Sumo-lings! Let’s set up your iBial account and AppSumo Code and you’ll<br />be ready to get your own live chat running!
            </div>
        </div>
        <div class="redeem-form">
            <?php if(isset($error)): ?>
                <div class="derrorpart"><?php echo $error['message']; ?></div>
            <?php endif; ?>
            <form action="<?php echo env('APP_URL'); ?>/redeem" method="post">
                {{ csrf_field() }}
                <div class="redeem-item">
                    <input type="text" name="demail" placeholder="Email">
                </div>
                <div class="redeem-item">
                    <input type="text" name="appcode" placeholder="AppSumo Code">
                </div>
                <div class="redeem-item">
                    <input type="submit" name="subs" value="Get Started">
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
