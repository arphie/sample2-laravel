<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oc_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('order_id'); // from OC
            $table->string('order_type'); // [non-reseller] or [reseller]
            $table->string('activation_count'); // if reseller, add number of tokens, if non-reseller, set to 1
            $table->string('invoice_no'); // from OC
            $table->string('store_id'); // from OC
            $table->string('customer_id'); // from OC
            $table->string('firstname'); // from OC
            $table->string('lastname'); // from OC
            $table->string('email'); // from OC
            $table->string('price'); // from OC
            $table->string('product_id'); // from OC
            $table->string('product_name'); // from OC
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oc_transactions');
    }
}
