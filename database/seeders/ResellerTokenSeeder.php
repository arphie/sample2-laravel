<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ResellerTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=0; $i < 10000; $i++) {
            DB::table('reseller_tokens')->insert([
                'code' => "Appsumo"."-rs-".Str::random(5)."-".sprintf('%06d', ($i+1))
            ]);
        }
    }
}
