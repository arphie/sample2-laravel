<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\AppsumoTokenModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AppsumoTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10000; $i++) {
            DB::table('appsumo_tokens')->insert([
                'code' => "Appsumo"."-".Str::random(10)."-".sprintf('%06d', ($i+1))
            ]);
        }
        // DB::table('users')->insert([
        //     'name' => Str::random(10),
        //     'email' => Str::random(10).'@gmail.com',
        //     'password' => Hash::make('password'),
        // ]);
    }
}
