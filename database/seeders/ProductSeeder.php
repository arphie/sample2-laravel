<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ch = curl_init();
        $url = "https://dev.ibial.com/store/index.php?route=api/product/getProducts";

        curl_setopt($ch, CURLOPT_URL, $url);
        // =========================== BOF certificate
        // $certificate_location = 'C:\Users\Celym\Downloads\cacert.pem';
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        // ============================ EOF certificate
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);

        $server_output = json_decode($server_output);

        foreach ($server_output as $key => $value) {
            DB::table('products')->insert([
                'product_id' => $value->product_id,
                'name' => $value->name,
                'description' => $value->description,
                'image' => $value->image,
                'price' => $value->price,
            ]);
        }

        //
        // for ($i=0; $i < 10000; $i++) {
        //     DB::table('reseller_tokens')->insert([
        //         'code' => "Appsumo"."-rs-".Str::random(5)."-".sprintf('%06d', ($i+1))
        //     ]);
        // }
    }
}
