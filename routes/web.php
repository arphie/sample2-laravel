<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    $redirect_to = env('APP_URL').'/login';
    return redirect($redirect_to);
});

Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// login function
Route::get('/login', 'AccountsController@login');
Route::post('/login', 'AccountsController@process_from_form');

Route::get('/login/forgot', 'AccountsController@forgot');
Route::post('/login/forgot', 'AccountsController@process_forgot');

Route::get('/logout', 'AccountsController@logout');
Route::post('/admin/licenses', 'AccountsController@change_domain');

Route::get('/redeem', 'AppSumoTokenController@convertionPage');
Route::post('/redeem', 'AppSumoTokenController@processConvertion');

Route::get('/redeem/success', 'AppSumoTokenController@processSuccess');

Route::get('/validate/reseller', 'AppSumoTokenController@validationPage');
Route::post('/validate/reseller', 'AppSumoTokenController@processValidationPage');

Route::get('/pullAppCodes', 'AppSumoTokenController@showpassform');
Route::post('/pullAppCodes', 'AppSumoTokenController@pullAppCodes');\



Route::group(['middleware' => 'usersession'], function () {
    // client pagew
    Route::get('/users/validate', 'RecordsController@validate_info');
    Route::post('/users/validate', 'RecordsController@validate_info_data');

    Route::get('/purchases/download/{product_id}', 'RecordsController@download_product');

    Route::get('/purchases', 'RecordsController@transaction_info');
    Route::get('/purchases/reseller', 'RecordsController@viewResellers');
    Route::get('/purchases/{id}', 'RecordsController@view_transaction');
    Route::view('/purchases/sample/single', 'client.single');
    Route::view('/purchases/sample/reseller', 'client.reseller');
    Route::get('/licenses/{id}/{page_id}/{update_to}', 'RecordsController@update_status_client');

});

Route::group(['middleware' => 'useradminsession'], function () {
    // client pagew
    Route::get('/admin/licenses', 'RegistrationConroller@view_tokens');
    Route::view('/admin/products', 'admin.products');
    Route::get('/admin/licenses/{id}/{update_to}', 'RecordsController@update_status');
});