<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// register api
Route::post("add", 'RegistrationConroller@add');


// register api
Route::post("update/{id}", 'RegistrationConroller@update');

Route::group([
    'prefix' => 'token',
], function () {
    // create token
    Route::post("create", 'RegistrationConroller@token_create');

    Route::post("check/email", 'RegistrationConroller@token_check_email');

    // check for token validity
    Route::post("check", 'RegistrationConroller@token_check');

    

    // check for token validity
    Route::get("all", 'RegistrationConroller@token_all');

    Route::group([
        'prefix' => 'transactions',
    ], function () {
        // check for token validity
        Route::post("save", 'RegistrationConroller@transaction_save');

        // insert from OC
        Route::post("insert", 'RegistrationConroller@insert_from_oc');

        // get transactions
        Route::post("transac", 'RegistrationConroller@transaction_proceed');

        // upgrade
        Route::post("improve", 'RegistrationConroller@improve');
    });
    
    
});

Route::group([
    'prefix' => 'records',
], function () {
    // get token
    Route::get("tokens", 'RecordsController@tokens');

    // get transactions
    Route::get("transaction", 'RecordsController@transaction');

    Route::post("monitor", 'RegistrationConroller@monitor');

    Route::group([
        'prefix' => 'free',
    ], function () {
        // check for token validity
        Route::post("activate", 'RegistrationConroller@free_activate');
    });

    Route::group([
        'prefix' => 'pro',
    ], function () {
        // check for token validity
        Route::post("activate", 'RegistrationConroller@pro_activate');
    });

    // Route::group([
    //     'prefix' => 'pro',
    // ], function () {
    //     // check for token validity
    //     Route::post("monitor", 'RegistrationConroller@monitor');
    // });
    
    // get user information
    Route::get("user/info", 'RecordsController@user_info');

    // get transaction information
    Route::get("transaction/info", 'RecordsController@transaction_information');
});

// getter
